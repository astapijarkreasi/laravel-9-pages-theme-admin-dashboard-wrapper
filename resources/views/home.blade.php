@extends('layouts.admin')

@section('content-body')
    <div class="view-port bg-success page-content-wrapper">
        <div class="view bg-danger-dark">
            <div class="p-2 d-flex justify-content-between bg-white"></div>
            <div class="mh-screen">
                <form role="form">
                    <div class="form-group form-group-default required">
                        <label class="highlight">Message</label>
                        <input type="text" class="form-control notification-message" placeholder="Type your message here"
                            value="This notification looks so perfect!" required>
                    </div>
                    <select class="cs-select cs-skin-slide notification-type" data-init-plugin="cs-select">
                        <option value="info">Info</option>
                        <option value="warning">Warning</option>
                        <option value="success">Success</option>
                        <option value="danger">Danger</option>
                        <option value="default">Default</option>
                    </select>
                    <button aria-label="" class="btn btn-success btn-lg show-notification">Show</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('addon-script')
    <script>
        $(document).ready(function() {
            $('.show-notification').click(function(e) {
                e.preventDefault();
                var button = $(this);
                var message = $('.notification-message')
            .val(); // Message to display inside the notification
                var type = $('select.notification-type').val(); // Info, Success, Error etc
                var position = $('.tab-pane.active .position.active').attr(
                'data-placement'); // Placement of the notification

                $('.page-content-wrapper').pgNotification({
                    style: 'bar',
                    message: message,
                    position: position,
                    timeout: 0,
                    type: type
                }).show();

                e.preventDefault();
            });

            $('.position').click(function() {
                $('.pgn').remove();
                $(this).closest('.notification-positions').find('.position').removeClass('active');
                $(this).addClass('active');
            });

            $('.btn-notification-style').click(function() {
                var target = $(this).attr('data-type');
                $('#preview-tab-position a[href="#' + target + '"]').tab('show')
            });

            // remove previously added notifications from the screen
            $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                var position = $(this).data('type');
                $('a[href="' + position + '"]').tab('show')
                $('.pgn').remove();
            });

        });
    </script>
@endpush
