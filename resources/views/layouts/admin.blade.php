<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
    @stack('addon-style')
    <style>
        .bg-dark-100 {
            background-color: #14213d;
        }
    </style>
</head>

<body class="fixed-header dashboard menu-pin">
    @include('includes.sidebar')

    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
        @include('includes.header')
        <!-- START PAGE CONTENT WRAPPER -->
        <div class="page-content-wrapper ">
            <!-- START PAGE CONTENT -->
            <div class="content sm-gutter">
                <!-- START JUMBOTRON -->
                <div data-pages="parallax">
                    <div class="container-fluid p-l-25 p-r-25 sm-p-l-0 sm-p-r-0">
                        <div class="inner">
                            <!-- START BREADCRUMB -->
                            <ol class="breadcrumb sm-p-b-5">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END JUMBOTRON -->
                <!-- START CONTAINER FLUID -->
                <div class="container-fluid p-l-25 p-r-25 p-t-0 p-b-25 sm-padding-10">
                    <!-- START ROW -->
                    <div class="row">
                        <div class="col-12">
                            <div id="card-circular" class="card widget-loader-bar m-b-10">
                                <div class="card-header ">
                                    <div class="card-title">Latihan
                                    </div>
                                    <div class="card-controls">
                                        <ul>
                                            <li><a href="#" class="card-collapse" data-toggle="collapse"><i
                                                        class="card-icon card-icon-collapse"></i></a>
                                            </li>
                                            <li><a href="#" class="card-refresh" data-toggle="refresh"><i
                                                        class="card-icon card-icon-refresh"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body" style="">
                                    Coba
                                    <x-button icon="edit" text="Edit" class="primary" type="submit"/>
                                    <x-input type="" label="Tanggal Lahir" placeholder=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="card no-border widget-loader-bar m-b-10">
                                <div class="container-xs-height full-height">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                        class="font-montserrat all-caps d-flex align-items-center">Weekly
                                                        Sales <i class="pg-icon">chevron_right</i>
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li><a href="#" class="portlet-refresh text-black"
                                                                data-toggle="refresh"><i
                                                                    class="portlet-icon portlet-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                                <h3 class="no-margin p-b-5">$24,000</h3>
                                                <span class="small hint-text pull-left">71% of total goal</span>
                                                <span class="pull-right small text-primary">$23,000</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-bottom">
                                            <div class="progress progress-small m-b-0">
                                                <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                                                <div class="progress-bar progress-bar-secondary" style="width:25%">
                                                </div>
                                                <!-- END BOOTSTRAP PROGRESS -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="card no-border widget-loader-bar m-b-10">
                                <div class="container-xs-height full-height">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                        class="font-montserrat all-caps d-flex align-items-center">Weekly
                                                        Sales <i class="pg-icon">chevron_right</i>
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li><a href="#" class="portlet-refresh text-black"
                                                                data-toggle="refresh"><i
                                                                    class="portlet-icon portlet-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                                <h3 class="no-margin p-b-5">$24,000</h3>
                                                <span class="small hint-text pull-left">71% of total goal</span>
                                                <span class="pull-right small text-primary">$23,000</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-bottom">
                                            <div class="progress progress-small m-b-0">
                                                <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                                                <div class="progress-bar progress-bar-warning" style="width:99%">
                                                </div>
                                                <!-- END BOOTSTRAP PROGRESS -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="card no-border widget-loader-bar m-b-10">
                                <div class="container-xs-height full-height">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                        class="font-montserrat all-caps d-flex align-items-center">Weekly
                                                        Sales <i class="pg-icon">chevron_right</i>
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li><a href="#" class="card-collapse"
                                                                data-toggle="collapse"><i
                                                                    class="card-icon card-icon-collapse"></i></a>
                                                        </li>
                                                        <li><a href="#" class="card-refresh"
                                                                data-toggle="refresh"><i
                                                                    class="card-icon card-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                                <h3 class="no-margin p-b-5">$24,000</h3>
                                                <span class="small hint-text pull-left">71% of total goal</span>
                                                <span class="pull-right small text-primary">$23,000</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-bottom">
                                            <div class="progress progress-small m-b-0">
                                                <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                                                <div class="progress-bar progress-bar-danger" style="width:50%">
                                                </div>
                                                <!-- END BOOTSTRAP PROGRESS -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="card no-border widget-loader-bar m-b-10">
                                <div class="container-xs-height full-height">
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="card-header  top-left top-right">
                                                <div class="card-title">
                                                    <span
                                                        class="font-montserrat all-caps d-flex align-items-center">Weekly
                                                        Sales <i class="pg-icon">chevron_right</i>
                                                    </span>
                                                </div>
                                                <div class="card-controls">
                                                    <ul>
                                                        <li><a href="#" class="portlet-refresh text-black"
                                                                data-toggle="refresh"><i
                                                                    class="portlet-icon portlet-icon-refresh"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-top">
                                            <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                                                <h3 class="no-margin p-b-5">$24,000</h3>
                                                <span class="small hint-text pull-left">71% of total goal</span>
                                                <span class="pull-right small text-primary">$23,000</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-xs-height">
                                        <div class="col-xs-height col-bottom">
                                            <div class="progress progress-small m-b-0">
                                                <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                                                <div class="progress-bar progress-bar-primary" style="width:71%">
                                                </div>
                                                <!-- END BOOTSTRAP PROGRESS -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END ROW -->
                    <div class="row md-m-b-10">
                        <div class="col-sm-3">
                            <!-- START WIDGET widget_pendingComments.tpl-->
                            <div class="card no-margin widget-loader-circle todolist-widget pending-projects-widget">
                                <div class="card-header ">
                                    <div class="card-title">
                                        <span class="d-flex align-items-center font-montserrat all-caps">
                                            Recent projects <i class="pg-icon">chevron_right</i>
                                        </span>
                                    </div>
                                    <div class="card-controls">
                                        <ul>
                                            <li><a href="#" class="card-collapse" data-toggle="collapse"><i
                                                        class="card-icon card-icon-collapse"></i></a>
                                            </li>
                                            <li><a href="#" class="card-refresh" data-toggle="refresh"><i
                                                        class="card-icon card-icon-refresh"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h5 class="">Ongoing projects at <span class="semi-bold">pages</span></h5>
                                    <ul class="nav nav-tabs nav-tabs-simple m-b-20" role="tablist"
                                        data-init-reponsive-tabs="collapse">
                                        <li class="nav-item"><a href="#pending" class="active" data-toggle="tab"
                                                role="tab" aria-expanded="true" aria-selected="true">Pending</a>
                                        </li>
                                        <li class="nav-item"><a href="#completed" data-toggle="tab" role="tab"
                                                aria-expanded="false" class=""
                                                aria-selected="false">Completed</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content no-padding">
                                        <div class="tab-pane active" id="pending">
                                            <div class="p-t-10">
                                                <div class="d-flex">
                                                    <span class="icon-thumbnail bg-contrast-low pull-left">kp</span>
                                                    <div class="flex-1 full-width overflow-ellipsis">
                                                        <p
                                                            class="hint-text all-caps font-montserrat fs-11 no-margin overflow-ellipsis ">
                                                            Revox Ltd
                                                        </p>
                                                        <h5 class="no-margin overflow-ellipsis ">Kepler - wordpress
                                                            builder</h5>
                                                    </div>
                                                </div>
                                                <div class="m-t-15">
                                                    <p class="hint-text small pull-left no-margin">71% completed from
                                                        total</p>
                                                    <a href="#" class="pull-right "><i
                                                            class="pg-icon">more_horizontal</i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="progress progress-small m-b-15 m-t-10">
                                                    <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                                                    <div class="progress-bar progress-bar-info" style="width:71%">
                                                    </div>
                                                    <!-- END BOOTSTRAP PROGRESS -->
                                                </div>
                                            </div>
                                            <div class="p-t-15">
                                                <div class="d-flex">
                                                    <span class="icon-thumbnail bg-warning-light pull-left ">cr</span>
                                                    <div class="flex-1 full-width overflow-ellipsis">
                                                        <p
                                                            class="hint-text all-caps font-montserrat fs-11 no-margin overflow-ellipsis ">
                                                            Nike Ltd
                                                        </p>
                                                        <h5 class="no-margin overflow-ellipsis ">Corporate rebranding
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="m-t-15">
                                                    <p class="hint-text small pull-left no-margin">20% completed from
                                                        total</p>
                                                    <a href="#" class="pull-right "><i
                                                            class="pg-icon">more_horizontal</i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="progress progress-small m-b-15 m-t-10">
                                                    <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                                                    <div class="progress-bar progress-bar-warning" style="width:20%">
                                                    </div>
                                                    <!-- END BOOTSTRAP PROGRESS -->
                                                </div>
                                            </div>
                                            <a href="#" class="btn btn-block m-t-30">See all projects</a>
                                        </div>
                                        <div class="tab-pane" id="completed">
                                            <div class="p-t-10">
                                                <div class="d-flex">
                                                    <span
                                                        class="icon-thumbnail bg-contrast-higher pull-left ">ws</span>
                                                    <div class="flex-1 full-width overflow-ellipsis">
                                                        <p
                                                            class="hint-text all-caps font-montserrat fs-11 no-margin overflow-ellipsis ">
                                                            Apple Corp
                                                        </p>
                                                        <h5 class="no-margin overflow-ellipsis ">Marketing Campaign for
                                                            revox</h5>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="m-t-15">
                                                    <p class="hint-text  small pull-left no-margin">45% completed from
                                                        total</p>
                                                    <a href="#" class="pull-right "><i
                                                            class="pg-icon">more_horizontal</i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="progress progress-small m-b-15 m-t-10">
                                                    <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                                                    <div class="progress-bar progress-bar-info" style="width:45%">
                                                    </div>
                                                    <!-- END BOOTSTRAP PROGRESS -->
                                                </div>
                                            </div>
                                            <div class="p-t-15">
                                                <div class="d-flex">
                                                    <span class="icon-thumbnail bg-primary-light pull-left ">cr</span>
                                                    <div class="flex-1 full-width overflow-ellipsis">
                                                        <p
                                                            class="hint-text all-caps font-montserrat fs-11 no-margin overflow-ellipsis ">
                                                            Yahoo Inc
                                                        </p>
                                                        <h5 class="no-margin overflow-ellipsis ">Corporate rebranding
                                                        </h5>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="m-t-15">
                                                    <p class="hint-text  small pull-left no-margin">20% completed from
                                                        total</p>
                                                    <a href="#" class="pull-right "><i
                                                            class="pg-icon">more_horizontal</i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="progress progress-small m-b-15 m-t-10">
                                                    <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                                                    <div class="progress-bar progress-bar-warning" style="width:20%">
                                                    </div>
                                                    <!-- END BOOTSTRAP PROGRESS -->
                                                </div>
                                            </div>
                                            <a href="#" class="btn btn-block m-t-30">See all projects</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET -->
                        </div>
                        <div class="col-6">
                            <div
                                class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle d-flex flex-column align-self-stretch">
                                <div class="card-header top-right">
                                    <div class="card-controls">
                                        <ul>
                                            <li><a data-toggle="refresh" class="portlet-refresh text-black"
                                                    href="#"><i
                                                        class="portlet-icon portlet-icon-refresh"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="padding-25">
                                    <div class="pull-left">
                                        <h2 class="text-success no-margin">Penjualan Hari Ini</h2>
                                        <p class="no-margin">Today's sales</p>
                                    </div>
                                    <h3 class="pull-right bold"><sup>
                                            <small class="semi-bold">Rp</small>
                                        </sup> 1,435,000
                                    </h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="auto-overflow widget-11-2-table">
                                    <table class="table table-condensed table-hover">
                                        <tbody>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345
                                                </td>
                                                <td class="text-right hidden-lg">
                                                    <span class="hint-text small">dewdrops</span>
                                                </td>
                                                <td class="text-right b-r b-dashed b-grey w-25">
                                                    <span class="hint-text small">Qty 1</span>
                                                </td>
                                                <td class="w-25">
                                                    <span class="font-montserrat fs-18">$27</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="padding-25 mt-auto">
                                    <p class="small no-margin">
                                        <a href="#"><i
                                                class="fa fs-16 fa-arrow-circle-o-down text-success m-r-10"></i></a>
                                        <span class="hint-text ">Show more details of APPLE . INC</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 sm-m-t-10">
                            <div id="card-circular" class="card card-default">

                                <div class="card-header ">
                                    <div class="card-title">Latihan
                                    </div>
                                    <div class="card-controls">
                                        <ul>
                                            <li><a href="#" class="card-collapse" data-toggle="collapse"><i
                                                        class="card-icon card-icon-collapse"></i></a>
                                            </li>
                                            <li><a href="#" class="card-refresh" data-toggle="refresh"><i
                                                        class="card-icon card-icon-refresh"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body" style="">
                                    Coba
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTAINER FLUID -->
            </div>
            <!-- END PAGE CONTENT -->
            <!-- START COPYRIGHT -->
            <!-- START CONTAINER FLUID -->
            <!-- START CONTAINER FLUID -->
            <div class=" container-fluid  container-fixed-lg footer">
                <div class="copyright sm-text-center">
                    <p class="small-text no-margin pull-left sm-pull-reset">
                        ©2022 All Rights Reserved. Hak Cipta oleh Light Project Bali.
                    </p>
                    <p class="small no-margin pull-right sm-pull-reset">
                        Bantuan
                    </p>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- END COPYRIGHT -->
        </div>
        @yield('content-body')
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    {{-- Quickview Session --}}

    {{-- @include('includes.script') --}}
    <!-- BEGIN VENDOR JS -->
    <script src="{{ asset('assets/plugins/feather-icons/feather.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <!--  A polyfill for browsers that don't support ligatures: remove liga.js if not needed-->
    <script src="{{ asset('assets/plugins/liga.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    {{-- <script src="{{ asset('assets/plugins/jquery/jquery-3.2.1.min.js') }}" type="text/javascript"></script> --}}
    <script src="{{ asset('assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/popper/umd/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/classie/classie.js') }}"></script>
    {{-- Pages --}}
    <script src="{{ asset('pages/js/pages.min.js') }}"></script>
    <script src="{{ asset('assets/js/card.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/scripts.js') }}" type="text/javascript"></script>
    @stack('addon-script')
</body>

</html>
