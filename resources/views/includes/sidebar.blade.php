<!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar bg-dark-100" data-pages="sidebar">

      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <div class="d-flex justify-content-center text-white font-bold py-3">
            <h5>Light</h5>
        </div>
        <hr class="my-1 border border-secondary">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-10">
            <a href="/">
              <span class="title">Dashboard</span>
            </a>
            <span class="icon-thumbnail"><i data-feather="shield"></i></span>
          </li>
          <li class="">
            <a href="email.html" class="detailed">
              <span class="title">Email</span>
              <span class="details">234 New Emails</span>
            </a>
            <span class="icon-thumbnail"><i data-feather="mail"></i></span>
          </li>
          <li class="">
            <a href="social.html"><span class="title">Social</span></a>
            <span class="icon-thumbnail"><i data-feather="users"></i></span>
          </li>
          <li>
            <a href="javascript:;"><span class="title">Calendar</span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail"><i data-feather="calendar"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="calendar.html">Basic</a>
                <span class="icon-thumbnail">c</span>
              </li>
              <li class="">
                <a href="calendar_lang.html">Languages</a>
                <span class="icon-thumbnail"><i data-feather="mail"></i></span>
              </li>
              <li class="">
                <a href="calendar_month.html">Month</a>
                <span class="icon-thumbnail">M</span>
              </li>
              <li class="">
                <a href="calendar_lazy.html">Lazy load</a>
                <span class="icon-thumbnail">La</span>
              </li>
              <li class="">
                <a href="https://docs.pages.revox.io/apps/calendar" rel="noreferrer" target="_blank">Documentation</a>
                <span class="icon-thumbnail">D</span>
              </li>
            </ul>
          </li>

          <li class="">
            <a href="http://changelog.pages.revox.io/" target="_blank"><span class="title">Changelog</span></a>
            <span class="icon-thumbnail">CG</span>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->
