<div>
    <button aria-label="Edit" class="btn btn-{{ $class }} btn-icon-left m-b-10" type="{{ !$type ? 'submit' : $type }}"><i class="pg-icon">{{ $icon }}</i><span class="">{{ $text }}</span></button>
</div>
