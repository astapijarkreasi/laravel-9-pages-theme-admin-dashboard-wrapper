<div>
    <div class="form-group form-group-default required">
        <label>{{ $label }}</label>
        <input type="{{ $type }}" class="form-control" placeholder="{{ $placeholder }}" required>
    </div>
</div>
