@extends('layouts.auth')

@section('content-body')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <style type="text/css">
        #results {
            padding: 20px;
            border: 1px solid;
            background: #ccc;
        }
    </style>
    <div class="container">
        <form method="POST" action="{{ route('webcam.capture') }}">
            @csrf
            <div class="card" id="camera_wrapper">
                <div class="card-body">
                    <div class="d-flex justify-content-center">
                        <div id="my_camera" class=""></div>
                    </div>
                    <br />
                    <input type=button value="Take Snapshot" onClick="take_snapshot()" class="btn btn-primary btn-block">
                    <input type="hidden" name="image" class="image-tag">

                </div>
            </div>
            <div class="card d-none" id="photo_wrapper">
                <div class="card-body">
                    <div id="results">Your captured image will appear here...</div>
                    <div id="post_take_buttons">
                        <input type=button value="&lt; Take Another" onClick="cancel_preview()">
                        <input type=button value="Save Photo &gt;" onClick="save_photo()" style="font-weight:bold;">
                    </div>
                        <button class="btn btn-success btn-block">Submit</button>
                </div>
            </div>


        </form>
    </div>

    <script language="JavaScript">
        var camera = document.getElementById("camera_wrapper");
        var photo = document.getElementById("photo_wrapper");
        // preload shutter audio clip
        var shutter = new Audio();
        shutter.autoplay = true;
        shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';
        Webcam.set({
            width: 320,
			height: 240,
			dest_width: 640,
			dest_height: 480,
            image_format: 'jpeg',
            jpeg_quality: 80,
            flip_horiz: true
        });
        Webcam.attach('#my_camera');

        function take_snapshot() {
            // play sound effect
            shutter.play();
            Webcam.snap(function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="' + data_uri + '"/>';
                camera.classList.add('d-none');
                photo.classList.remove('d-none');
            });
        }

        function cancel_preview() {
			// cancel preview freeze and return to live camera feed
			Webcam.unfreeze();

			// swap buttons back
			document.getElementById('pre_take_buttons').style.display = '';
			document.getElementById('post_take_buttons').style.display = 'none';
		}
    </script>
@endsection
