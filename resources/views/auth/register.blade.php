@extends('layouts.auth')

@section('content-header')
Daftar Pengguna Baru
@endsection

@section('content-body')
<h3 class="mb-0"><span class="semi-bold">Daftar ke </span> Light Project</h3>
    <p>Lengkapi data berikut ini</p>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-group-default">
                    <label>Nama Pengguna</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-group-default">
                    <label>Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-group-default">
                    <label>Kata Sandi</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                </div>
            </div>
        </div>
        <div class="row m-t-10">
            <div class="col-lg-6">
                <p><small>Sudah memiliki akun? <a href="{{ route('login') }}">Login</a>.</small></p>
            </div>
            <div class="col-lg-6 text-right">
                <a href="#" class="text-info small">Lupa Kata Sandi?</a>
            </div>
        </div>
        <button aria-label="" class="btn btn-primary btn-block btn-cons m-t-10 py-2" type="submit">Masuk ke Sysfo</button>
    </form>
@endsection
