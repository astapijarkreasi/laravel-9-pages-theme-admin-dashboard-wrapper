@extends('layouts.auth')
@section('content-header')
    Login
@endsection
@section('content-body')
    <h2 class="mw-80">Get started with your account.</h2>
    <p class="fs-16 mw-80 m-b-20">Find your people. Engage your customers. Build your brand. Do it all with Page's UI
        Framework Platform.</p>

    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="row form-group-attached">
            <div class="col-md-12">
                <div class="form-group form-group-default">
                    <label>Email</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-group-default">
                    <label>Kata Sandi</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row m-t-25">
            <div class="col-xl-6 p-b-10">
                <p class="small-text hint-text">Anda pengguna baru? <a href="#">Daftar disini!</a> atau klik <a
                        href="#">Bantuan</a> jika anda <b>lupa kata sandi</b> Anda.</p>
            </div>
            <div class="col-xl-6">
                <button aria-label="" data-toggle="refresh" class="btn btn-primary pull-right btn-lg btn-block" type="submit">Login
                </button>
            </div>
        </div>
    </form>

    <a class="btn btn-link btn-icon btn-block">Login dengan Google</a>

@endsection
