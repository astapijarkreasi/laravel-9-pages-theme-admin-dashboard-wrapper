<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{

    public $type;
    public $label;
    public $placeholder;

    public function __construct($type, $label, $placeholder){
        $this->type        = $type;
        $this->label       = $label;
        $this->placeholder = $placeholder;
    }

    public function render()
    {
        return view('components.input');
    }
}
