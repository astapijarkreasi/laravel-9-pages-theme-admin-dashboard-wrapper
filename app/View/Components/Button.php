<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Button extends Component
{
    public $icon;
    public $type;
    public $text;
    public $class;
    public function __construct( $icon, $type, $text, $class)
    {
        $this->icon  = $icon;
        $this->type  = $type;
        $this->text  = $text;
        $this->class = $class;
    }

    public function render()
    {
        return view('components.button');
    }
}
